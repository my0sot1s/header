#!/bin/bash
# #build for common
protoc common/common.proto -I. --go_out=../../../

protoc -I/usr/local/include -I. -I$GOPATH/src \
	account/account.proto seat/seat.proto \
	-I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	--swagger_out=logtostderr=true,allow_merge=true:.

# protoc account/account.proto -I. --go_out=plugins=grpc:.
#  --go_out=plugins=grpc:.
protoc -I/usr/local/include -I. -I$GOPATH/src \
  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	--go_out=plugins=grpc:. account/account.proto

# #  --go_out=plugins=grpc:.
# protoc -I/usr/local/include -I. -I$GOPATH/src \
#   -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
# 	--go_out=plugins=grpc:. service/service.proto

# protoc user/user.proto -I. --go_out=plugins=grpc:.

# protoc store/store.proto -I. --go_out=plugins=grpc:.

protoc  -I/usr/local/include -I. -I$GOPATH/src \
  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	--go_out=plugins=grpc:. seat/seat.proto


./convert account/account.pb.go seat/seat.pb.go
# copy file
echo copy file
cp apidocs.swagger.json ../devconfigs
echo all done!