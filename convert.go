package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	// readFile
	fmt.Println("processing: ", os.Args[1:])
	for _, path := range os.Args[1:] {
		file := strings.Split(path, "/")
		body, _ := ioutil.ReadFile(path)
		fileContentReplaced := strings.Replace(string(body), "package _", "package "+file[0], 1)
		if err := ioutil.WriteFile(path, []byte(fileContentReplaced), 0644); err != nil {
			fmt.Print(err)
		}
		fmt.Println(path, " Done!")
	}
}
